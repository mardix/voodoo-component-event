<?php

/**
 * An Event logger. Requires Raven_Client
 * @link https://github.com/getsentry/raven-php
 * 
 * @name Log
 * @author Mardix
 * @since   Aug 5, 2013
 */

namespace Voodoo\Component\Event;

use Exception,
    ErrorException,
    Raven_ErrorHandler;

class Log extends LogClient
{
    const DEBUG = 'debug';
    const INFO = 'info';
    const WARN = 'warning';
    const WARNING = 'warning';
    const ERROR = 'error';
    const FATAL = 'fatal';

    private $eventId = null;

    /**
     * Log message
     * 
     * @param type $message
     * 
     * @param array $params
     * @param type $level
     * @return \Voodoo\Component\Logger\Event
     */
    public function message($message, $params = [], $level = self::INFO)
    {
        $this->eventId = $this->getClient()
                                ->captureMessage($message, $params, $level);
        return $this;
    }

    /**
     * Capture the query
     * 
     * @param string $query
     * @param string $engine
     * @param sting $level
     * 
     * @return \Voodoo\Component\Logger\Event
     */
    public function query($query, $engine = "", $level = self::DEBUG)
    {
        $this->eventId = $this->getClient()
                                ->captureQuery($query, $level, $engine);
        return $this;        
    }
    
    
    /**
     * Log an exception
     * 
     * @param Exception $e
     * @return \Voodoo\Component\Logger\Log
     */
    public function exception(Exception $e)
    {
        $this->eventId = $this->getClient()
                                ->captureException($e);
        return $this;
    }    

    public function error($message, $code = 0, $file = '', $line = 0)
    {
        $this->exception(new ErrorException($message, 0, $code, $file, $line));
        return $this;
    }

    /**
     * Return the event Id
     * 
     * @return string
     */
    public function getId()
    {
        return $this->eventId;
    }
    
    
    public function setTags(Array $tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * Catch all errors
     * @param bool $registerGenericError - to register generic error. It will be noisey
     */
    public static function catchAllErrors($registerGenericError = true)
    {
        $error_handler = new Raven_ErrorHandler((new self)->getClient());
        $error_handler->registerExceptionHandler();
        $error_handler->registerShutdownFunction();
        
        if ($registerGenericError) {
            $error_handler->registerErrorHandler();
        }
    }  
}
