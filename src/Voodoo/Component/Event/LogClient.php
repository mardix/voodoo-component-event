<?php

/**
 * Raven Client
 * @link https://github.com/getsentry/raven-php
 * 
 * @name Client
 * @author Mardix
 * @since   Aug 5, 2013
 */

namespace Voodoo\Component\Event;

use Voodoo,
    Raven_Client;


class LogClient
{
    private $_ravenClient = null;
    private static $ravenClient = null;
    private $sentryDsn = null;
    private $sentryParams = [];

    
    public function __construct($sentryDsn = null)
    {
        $this->sentryParams = [
            "logger" => "php",
            "tags" => 
                [
                    "hostname" => Voodoo\Core\Env::getHostName(),
                    "voodoophp_version" => Voodoo\Core\Application::VERSION,                   
                ]
            ];
        
        $this->sentryDsn = $sentryDsn;
    }
    
    
    /**
     * Return the client
     * 
     * @return Raven_Client
     */
    public function getClient()
    {
        if ($this->sentryDsn) {
            if (!$this->_ravenClient) {
                $this->_ravenClient = new Raven_Client($this->sentryDsn, $this->sentryParams);
            }
            return $this->_ravenClient;
        } else {
            if (! self::$ravenClient) {
                $dsn = Voodoo\Core\Config::Component()->get("EventLogger.logSentryDsn");
                self::$ravenClient = new Raven_Client($dsn, $this->sentryParams);
            }
            return self::$ravenClient;            
        }
    }
}
